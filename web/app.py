from flask import Flask, request, render_template, abort, redirect
import os
import sys

app = Flask(__name__)

@app.route("/<path:path>", methods=["GET"])
def index(path):
    app.logger.info("Attempt to access page: " + path)
    if "~" in path or "//" in path or ".." in path:
        # Forbidden characters
        abort(403)
    else:
        # Flask defaults to templates/ for our pages
        # So we should be fine to hardcode this
        url = "templates/" + path
        if os.path.exists(url) and not os.path.isdir(url):
            # Page exists
            return render_template(path, resource=path)
        else:
            # Can't find page or might be a directory
            abort(404)

@app.errorhandler(404)
def fnf(e):
    # Send file not found page
    return render_template("404.html", msg=str(e)), 404

@app.errorhandler(403)
def forbidden(e):
    # Send access forbidden page
    return render_template("403.html", msg=str(e)), 403

@app.route("/", methods=["GET"])
def landing():
    # Default to index.html for landing page
    return redirect("index.html")

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
